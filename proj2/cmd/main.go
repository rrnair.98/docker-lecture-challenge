package main

import (
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
)

func main() {

	router := gin.Default()

	router.GET("/hello/", SayHello)

	err := router.Run("0.0.0.0:8099")
	if err != nil {
		panic(err)
	}

}

func SayHello(c *gin.Context) {
	log.Println("processing request")
	c.JSON(http.StatusOK, gin.H{"response": "world"})

}
