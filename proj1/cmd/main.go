package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"io"
	"log"
	"net/http"
	"os"
)

var HelloServer = GetFromEnvOrDefault("HELLO_HOST", "0.0.0.0")

const HELLO_FMT = "http://%s:8099/hello"

func main() {

	router := gin.Default()

	router.GET("/say-hello/", SayHello)

	router.Run("0.0.0.0:8090")

}

func GetFromEnvOrDefault(key string, def string) string {
	val, found := os.LookupEnv(key)
	if !found {
		return def
	}
	return val
}

func SayHello(c *gin.Context) {
	log.Println("processing request")
	url := fmt.Sprintf(HELLO_FMT, HelloServer)
	client := &http.Client{}
	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		log.Println("Error creating request:", err)
		c.JSON(http.StatusInternalServerError, gin.H{"error": err})
		return
	}

	// Send the request and get the response
	resp, err := client.Do(req)
	if err != nil {
		log.Println("Error sending request:", err)
		c.JSON(http.StatusInternalServerError, gin.H{"error": err})
		return
	}
	defer resp.Body.Close()

	bodyBytes, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Println("Error reading response body:", err)
		c.JSON(http.StatusInternalServerError, gin.H{"error": err})
		return
	}
	// Convert the body bytes to a string (optional)
	responseBody := string(bodyBytes)
	log.Println("responseBody:", responseBody)
	c.JSON(http.StatusOK, gin.H{"response": responseBody})

}
