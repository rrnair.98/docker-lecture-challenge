# Task
Your task is to create 2 dockerfiles for each of these projects and ensure that these projects can talk to each other.
# Pre-requisites
You must know the following
* docker
* docker networking
* docker compose
# PreWord
## Project1
This is a webserver that runs itself on port 8090. It exposes a GET endpoint `/say-hello/`. 
It fires a request to Project2 on the /hello endpoint and forwards the response on this stream.
## Project2
This is a webserver that runs itself on port 8099. It exposes a GET endpoint `/hello/` that responds with
```json
{"response":"world"}
```
# Rules
* You are not allowed to change the source code.
* You are expected to create 2 dockerfiles such that Proj1 is exposed on its port and can consume data off of Proj2
and show the execution without using docker-compose
* You are expected to create a docker-compose  file for the same and show sample execution
expected response
```json
{"response":"{\"response\":\"world\"}"}
```